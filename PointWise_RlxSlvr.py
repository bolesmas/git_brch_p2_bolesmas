# Matrix-free Jacobi relaxation

import numpy as np
import time as tm


# number of grids
gpt = 201

T = np.zeros(gpt)*0
T_old = np.zeros(gpt)


# boundary condition
T[0] = 400
T[gpt-1] = 10

resd = np.zeros(gpt-1)

nrm = []

# record time before iteration
tbg = tm.time()

iter = 1
for iter in range(100000):

    T_old[0:gpt] = T[0:gpt]
 
    #for i in range(1,gpt-1):
    #    T[i] = (0 - (T_old[i-1]+T_old[i+1]))/(-2)

    #for i in range(1,gpt-1):
    #    resd[i] = T[i-1]-2*T[i]+T[i+1]
    T[1:gpt-1]=(T_old[0:gpt-2]+T_old[2:gpt])/2.0
    resd[1:gpt-1]=T[0:gpt-2]-2.0*T[1:gpt-1]+T[2:gpt]
    
    nrm.append(np.linalg.norm(resd[1:gpt-1],2))
    

    if nrm[-1] < 1.0e-4:
        break


# record time before iteration
ted = tm.time()

# print the result
print(iter,ted-tbg,nrm[-1])
